<?php

use Neon\News\Models\News;

return [

  /** Media collections defines places where the related media will be used.
   *
   */
  'collections'   => [

    /**
     * 
     */ 
    News::MEDIA_HEADER   => [],
    
    News::MEDIA_CONTENT  => []
  ],

  /** Media conversations.
   * 
   */

];
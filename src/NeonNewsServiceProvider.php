<?php

namespace Neon\News;

use Illuminate\Foundation\Console\AboutCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class NeonNewsServiceProvider extends PackageServiceProvider
{
  const VERSION = '1.0.0-alpha-8';

  public function configurePackage(Package $package): void
  {
    AboutCommand::add('N30N', 'News', self::VERSION);

    $package
      ->name('neon-news')
      ->hasConfigFile()
      ->hasViews()
      ->hasMigration('create_news_table');
  }
}

?>